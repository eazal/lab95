# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130807200814) do

  create_table "answers", :force => true do |t|
    t.string   "answer"
    t.integer  "user_id"
    t.integer  "question_id"
    t.integer  "correct"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
    t.integer  "mod_id"
    t.integer  "modulegroup_id"
    t.string   "meta"
    t.string   "ip"
    t.integer  "modulegroup_mod_id"
  end

  create_table "charts", :force => true do |t|
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "clicks", :force => true do |t|
    t.integer  "xval"
    t.integer  "yval"
    t.integer  "question_id"
    t.integer  "answer_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "images", :force => true do |t|
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
    t.string   "filepicker_url"
    t.integer  "mod_id"
  end

  create_table "mods", :force => true do |t|
    t.string   "name"
    t.string   "mtype"
    t.string   "filepicker_url"
    t.boolean  "archived"
    t.boolean  "graded"
    t.integer  "user_id"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
    t.integer  "key_module_group"
    t.string   "filepicker_url_alt"
  end

  create_table "modulegroup_mods", :force => true do |t|
    t.integer  "mod_rank"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
    t.integer  "mod_id"
    t.integer  "modulegroup_id"
  end

  create_table "modulegroups", :force => true do |t|
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.string   "name"
    t.integer  "user_id"
  end

  create_table "options", :force => true do |t|
    t.string   "name"
    t.integer  "question_id"
    t.string   "filepicker_url"
    t.boolean  "correct"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "questions", :force => true do |t|
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
    t.text     "name"
    t.text     "qtype"
    t.string   "filepicker_url"
    t.boolean  "shuffle"
    t.integer  "mod_id"
    t.integer  "score"
    t.boolean  "archived"
    t.boolean  "optional"
    t.string   "description"
  end

  create_table "submissions", :force => true do |t|
    t.integer  "user_id"
    t.integer  "survey_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.text     "ip"
    t.string   "meta"
    t.boolean  "completed"
    t.integer  "study_id"
    t.integer  "trial_id"
    t.integer  "score"
    t.integer  "answer_id"
  end

  create_table "tag_links", :id => false, :force => true do |t|
    t.integer "module_id"
    t.integer "tag_id"
  end

  create_table "tags", :force => true do |t|
    t.string   "name"
    t.integer  "user_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "team_links", :id => false, :force => true do |t|
    t.integer "team_id"
    t.integer "user_id"
  end

  create_table "teams", :force => true do |t|
    t.string   "name"
    t.string   "filepicker_url"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "users", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.integer  "level"
    t.string   "filepicker_url"
    t.boolean  "tourTaken"
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

end

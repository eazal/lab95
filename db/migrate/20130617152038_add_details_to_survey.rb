class AddDetailsToSurvey < ActiveRecord::Migration
  def change
    add_column :surveys, :allow_guests, :boolean
  end
end

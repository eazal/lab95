class CreateClicks < ActiveRecord::Migration
  def change
    create_table :clicks do |t|
      t.integer :xval
      t.integer :yval
      t.integer :question_id
      t.integer :answer_id

      t.timestamps
    end
  end
end

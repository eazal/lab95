class AddDetailsToSubmissions23940 < ActiveRecord::Migration
  def change
    add_column :submissions, :trial_id, :integer
    add_column :submissions, :score, :integer
  end
end

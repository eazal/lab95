class RenameType < ActiveRecord::Migration
  def up
    rename_column :surveys, :type, :kind
  end

  def down
    rename_column :surveys, :kind, :type
  end
end

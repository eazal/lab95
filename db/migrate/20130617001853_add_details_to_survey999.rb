class AddDetailsToSurvey999 < ActiveRecord::Migration
  def change
    add_column :surveys, :responses_submitted, :integer
    add_column :surveys, :responses_max, :integer
  end
end

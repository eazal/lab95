class CreateTableCollectionLinks < ActiveRecord::Migration
  def up
    create_table(:collection_links, id:false) do |t|
      t.column :collection_id, :integer
      t.column :order, :integer
      t.column :module_id, :integer
    end
  end

  def down
  end
end

class ChangeTextFormatInSurveys < ActiveRecord::Migration
  def up
   change_column :surveys, :published, :boolean
  end

  def down
   change_column :surveys, :published, :text
  end
end

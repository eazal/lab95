class CreateModulegroupMods < ActiveRecord::Migration
  def change
    create_table :modulegroup_mods do |t|
      t.integer :rank

      t.timestamps
    end
  end
end

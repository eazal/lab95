class CreateTrialsCollections < ActiveRecord::Migration
  def change
    create_table :trials_collections, :id => false do |t|
      t.integer :trial_id
      t.integer :collection_id
    end
  end
end

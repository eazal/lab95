class CreateTableTeamUser < ActiveRecord::Migration
  def up
    create_table(:team_links, id:false) do |t|
      t.column :team_id, :integer
      t.column :user_id, :integer
    end
  end

  def down
  end
end

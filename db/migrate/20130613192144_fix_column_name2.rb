class FixColumnName2 < ActiveRecord::Migration
   def change
      rename_column :surveys, :question, :description
   end
end

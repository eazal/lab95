class AddDetailsToQuestions2334 < ActiveRecord::Migration
  def change
    add_column :questions, :score, :integer
    add_column :questions, :archived, :boolean
    add_column :questions, :optional, :boolean
  end
end

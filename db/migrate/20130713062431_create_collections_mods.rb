class CreateCollectionsMods < ActiveRecord::Migration
   create_table :collections_mods, :id => false do |t|
      t.integer :collection_id
      t.integer :mod_id
   end
end

class CreateTrials < ActiveRecord::Migration
  def change
    create_table :trials do |t|
      t.string :name
      t.boolean :archived
      t.integer :collection_id

      t.timestamps
    end
  end
end

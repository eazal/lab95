class CreateTableTagLinks < ActiveRecord::Migration
  def up
    create_table(:tag_links, id:false) do |t|
      t.column :module_id, :integer
      t.column :tag_id, :integer
    end
  end

  def down
  end
end

class CreateAnswers < ActiveRecord::Migration
  def change
    create_table :answers do |t|
      t.integer :submission_id
      t.string :answer
      t.integer :user_id
      t.integer :question_id
      t.integer :correct

      t.timestamps
    end
  end
end

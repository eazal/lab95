class CreateMods < ActiveRecord::Migration
  def change
    create_table :mods do |t|
      t.string :name
      t.string :type
      t.string :filepicker_url
      t.boolean :archived
      t.boolean :graded
      t.integer :user_id

      t.timestamps
    end
  end
end

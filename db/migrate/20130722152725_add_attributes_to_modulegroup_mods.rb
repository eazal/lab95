class AddAttributesToModulegroupMods < ActiveRecord::Migration
  def change
    add_column :modulegroup_mods, :mod_id, :integer
    add_column :modulegroup_mods, :modulegroup_id, :integer
  end
end

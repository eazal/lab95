class CreateStudies < ActiveRecord::Migration
  def change
    create_table :studies do |t|
      t.text :name
      t.text :description
      t.references :user

      t.timestamps
    end
    add_index :studies, :user_id
  end
end

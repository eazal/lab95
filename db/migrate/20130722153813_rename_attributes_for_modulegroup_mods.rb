class RenameAttributesForModulegroupMods < ActiveRecord::Migration
  def change
    rename_column :modulegroup_mods, :rank, :mod_rank
  end
end

class DeleteSubmissionLinkToResponses < ActiveRecord::Migration
  def change
    remove_column :answers, :submission_id
  end
end

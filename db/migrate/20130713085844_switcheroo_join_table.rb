class SwitcherooJoinTable < ActiveRecord::Migration
  def change
    rename_table :trials_collections, :collections_trials
  end
end

class AddDetailsToResponses < ActiveRecord::Migration
  def change
    add_column :responses, :answer, :text
  end
end

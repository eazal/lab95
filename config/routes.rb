Surveyrunner::Application.routes.draw do

  get "help/index"

  root :to => "home#index"

  match '/check' => 'users#check'

  # Ghost-in as user
  match '/ghost/:user_id' => 'home#ghost'

  # Rerouting for ajax devise login
  devise_for :users, :controllers => { :sessions => 'sessions' }

  match "help" => "help#index"
  match "users/all" => "users#all"
  match "users/:id/edit" => "users#edit", :as => :user
  match "users/:id/" => "users#update", :via => :put

  # For retrieving plain JSON
  scope(:path => '/raw') do
    resources :mods do
      resources :questions do
        resources :answers
        resources :options
      end
    end
    resources :modulegroups do
      resource :modulegroup_mods
    end
    resources :modulegroup_mods do
      resources :mods
    end
  end
  resources :modulegroups
  match "raw/questions/:id/charts" => "answers#chart"

  # For AngularJS partials
  scope(:path => '/partials') do
    match "index.html" => 'home#default'
    match "mod-detail.html" => 'mods#mod_detail'
    match "mod-new.html" => 'mods#mod_new'
    match "modulegroup-detail.html" => 'modulegroups#modulegroup_detail'
  end

  match '/raw/*something' => redirect('/')

  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end

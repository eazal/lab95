class StudySerializer < ActiveModel::Serializer
  attributes :id, :name, :description
  has_many :surveys
  # Doesn't seem to be working yet.
  # has_many :questions, :through => :surveys
end

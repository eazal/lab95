class Click < ActiveRecord::Base
  attr_accessible :answer_id, :question_id, :xval, :yval
  belongs_to :question
  belongs_to :answer
end

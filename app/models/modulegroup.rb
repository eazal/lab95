class Modulegroup < ActiveRecord::Base
  attr_accessible :name, :user_id
  belongs_to :user
  has_many :modulegroup_mods
  has_many :mods, :through => :modulegroup_mods
  has_many :answers
end
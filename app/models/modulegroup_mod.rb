class ModulegroupMod < ActiveRecord::Base
  attr_accessible :mod_rank, :modulegroup_id, :mod_id

  belongs_to :modulegroup
  belongs_to :mod

  # From https://github.com/mixonic/ranked-model#complex-use
  include RankedModel
  ranks :mod_rank,
    :with_same => :mod_id

end
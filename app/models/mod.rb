class Mod < ActiveRecord::Base
  attr_accessible :archived, :filepicker_url, :filepicker_url_alt, :graded, :name, :mtype, :user_id, :key_module_group
  after_initialize :init
  belongs_to :user 
  has_many :questions, :dependent => :destroy
  accepts_nested_attributes_for :questions

  has_many :modulegroup_mods, :dependent => :destroy
  has_many :images
  has_many :modulegroups, :through => :modulegroup_mods

  def init
    self.archived ||= false if self.archived.nil?
    self.graded ||= false if self.graded.nil?
  end
end
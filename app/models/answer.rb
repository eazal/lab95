class Answer < ActiveRecord::Base
  attr_accessible :answer, :correct, :question_id, :submission_id, :user_id, :modulegroup_id, :mod_id, :ip, :meta, :modulegroup_mod_id
  has_one :submission
  has_one :modulegroup
  belongs_to :question
  has_one :user
  has_many :clicks
end

class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :trackable, :validatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me, :tourTaken
  after_initialize :init
  after_create :create_sample_data

  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me, :level, :filepicker_url
  # attr_accessible :title, :body
  has_many :mods
  has_many :modulegroups
  belongs_to :answer

  def init
    self.level ||= 0 if self.level.nil?
  end

  def create_sample_data
    # Five second test followup
    sample_survey_modulegroup = Modulegroup.create(:user_id => self.id)
    sample_survey_modulegroup.save()

    sample_survey = Mod.create(:name => 'Five Second Test: Followup', :user_id => self.id , :mtype => 'ps', :key_module_group => sample_survey_modulegroup.id)
    sample_survey.save()

    sample_survey_modulegroup_mod = ModulegroupMod.create(:mod_id => sample_survey.id, :modulegroup_id => sample_survey_modulegroup.id)
    sample_survey_modulegroup_mod.save()

    sample_survey_question1 = Question.create(:mod_id => sample_survey.id, :name => 'Regarding the logo you just saw, how trustworthy do you think this business is?', :qtype => 'radio')
    sample_survey_question1.save()
    sample_survey_question2 = Question.create(:mod_id => sample_survey.id, :name => 'What typeface would you rather see?', :qtype => 'radio')
    sample_survey_question2.save()

    sample_survey_question1_option1 = Option.create(:name => 'Very trustworthy', :question_id => sample_survey_question1.id)
    sample_survey_question1_option1.save()
    sample_survey_question1_option2 = Option.create(:name => 'Somewhat trustworthy', :question_id => sample_survey_question1.id)
    sample_survey_question1_option2.save()
    sample_survey_question1_option3 = Option.create(:name => 'Neutral', :question_id => sample_survey_question1.id)
    sample_survey_question1_option3.save()
    sample_survey_question1_option4 = Option.create(:name => 'Somewhat not trustworthy', :question_id => sample_survey_question1.id)
    sample_survey_question1_option4.save()
    sample_survey_question1_option5 = Option.create(:name => 'Not at all trustworthy', :question_id => sample_survey_question1.id)
    sample_survey_question1_option5.save()

    sample_survey_question2_option1 = Option.create(:filepicker_url=> 'https://www.filepicker.io/api/file/OGEqxa7bQ1isaFtMtyFu', :question_id => sample_survey_question2.id)
    sample_survey_question2_option1.save()
    sample_survey_question2_option2 = Option.create(:filepicker_url=> 'https://www.filepicker.io/api/file/qmWMhvTISUQBlsHEaqpg', :question_id => sample_survey_question2.id)
    sample_survey_question2_option2.save()
    sample_survey_question2_option2 = Option.create(:filepicker_url=> 'https://www.filepicker.io/api/file/JcxKf1HNSHWLVplteL0w', :question_id => sample_survey_question2.id)
    sample_survey_question2_option2.save()

    # Five second test logo
    sample_fst_modulegroup = Modulegroup.create(:user_id => self.id)
    sample_fst_modulegroup.save()

    sample_fst = Mod.create(:name => 'Five Second Test: Logo', :user_id => self.id , :mtype => 'fs', :key_module_group => sample_fst_modulegroup.id, :filepicker_url => 'https://www.filepicker.io/api/file/NEq8sOZmTMyCA71G3tIV')
    sample_fst.save()

    sample_fst_modulegroup_mod = ModulegroupMod.create(:mod_id => sample_fst.id, :modulegroup_id => sample_fst_modulegroup.id)
    sample_fst_modulegroup_mod.save()

    # A/B Homepage
    sample_ab_modulegroup = Modulegroup.create(:user_id => self.id)
    sample_ab_modulegroup.save()

    sample_ab = Mod.create(:name => 'A/B Homepage', :user_id => self.id , :mtype => 'ab', :key_module_group => sample_ab_modulegroup.id, :filepicker_url => 'https://www.filepicker.io/api/file/dnIQ9nS4RPurqdKAs4gq', :filepicker_url_alt => 'https://www.filepicker.io/api/file/yzlmNMzTTmCpFpL1ZDgQ')
    sample_ab.save()

    sample_ab_question1 = Question.create(:mod_id => sample_ab.id, :name => 'What type of industries use this site (please guess)?', :qtype => 'checkbox')
    sample_ab_question1.save()
    sample_ab_question2 = Question.create(:mod_id => sample_ab.id, :name => 'What type of services does this website offer?', :qtype => 'input')
    sample_ab_question2.save()

    sample_ab_question1_option1 = Option.create(:name => 'Health', :question_id => sample_ab_question1.id)
    sample_ab_question1_option1.save()
    sample_ab_question1_option2 = Option.create(:name => 'Finance', :question_id => sample_ab_question1.id)
    sample_ab_question1_option2.save()
    sample_ab_question1_option3 = Option.create(:name => 'Marketing', :question_id => sample_ab_question1.id)
    sample_ab_question1_option3.save()
    sample_ab_question1_option4 = Option.create(:name => 'Business Development', :question_id => sample_ab_question1.id)
    sample_ab_question1_option4.save()
    sample_ab_question1_option5 = Option.create(:name => 'Technology', :question_id => sample_ab_question1.id)
    sample_ab_question1_option5.save()
    sample_ab_question1_option6 = Option.create(:name => 'Accounting', :question_id => sample_ab_question1.id)
    sample_ab_question1_option6.save()
    sample_ab_question1_option7 = Option.create(:name => 'Legal', :question_id => sample_ab_question1.id)
    sample_ab_question1_option7.save()

    sample_ab_modulegroup_mod = ModulegroupMod.create(:mod_id => sample_ab.id, :modulegroup_id => sample_ab_modulegroup.id)
    sample_ab_modulegroup_mod.save()

    # Modulegroup for them all
    sample_all_modulegroup = Modulegroup.create(:name => 'Vandelay Redesign', :user_id => self.id)
    sample_all_modulegroup.save()

    sample_all_ab_modulegroup_mod = ModulegroupMod.create(:mod_id => sample_ab.id, :modulegroup_id => sample_all_modulegroup.id)
    sample_all_ab_modulegroup_mod.save()
    sample_all_fst_modulegroup_mod = ModulegroupMod.create(:mod_id => sample_fst.id, :modulegroup_id => sample_all_modulegroup.id)
    sample_all_fst_modulegroup_mod.save()
    sample_all_survey_modulegroup_mod = ModulegroupMod.create(:mod_id => sample_survey.id, :modulegroup_id => sample_all_modulegroup.id)
    sample_all_survey_modulegroup_mod.save()
  end

end

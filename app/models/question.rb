class Question < ActiveRecord::Base
  validates :mod_id, :presence => true
  belongs_to :mod
  has_many :answers, :dependent => :destroy
  has_many :options, :dependent => :destroy
  has_many :clicks
  has_one :chart
  attr_accessible :name, :qtype, :archived, :filepicker_url, :options, :score, :mod_id, :description
  accepts_nested_attributes_for :answers
  after_initialize :init
  def init
    self.archived ||= false if self.archived.nil?
  end
  def count_score
    self.score = (
      self.answers.where('answer = ?', 1).count - self.answers.where('answer = ?', -1).count
    )
  end
end

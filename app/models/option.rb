class Option < ActiveRecord::Base
  attr_accessible :correct, :filepicker_url, :name, :question_id
  belongs_to :question
end

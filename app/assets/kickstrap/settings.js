// APPS
// ====

ks.apps = [
	'tinygrowl'
	, 'bootstrap'	
]

// BASIC SITE SETTINGS
// ===================

ks.settings = {

	root: ''
	// Location of the "kickstrap" folder.

	, mode: 'development'
	// Choose "development" or "production"

}


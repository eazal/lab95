var allContacts = [
	{ 
		img: '8/86/Charlotte_Bronte_coloured_drawing.png'
    , bio: 'Charlotte Bronte was an English novelist and poet, the eldest of the three Brontë sisters who survived into adulthood, whose novels are English literature standards. She wrote Jane Eyre under the pen name Currer Bell.'
		, fName: 'Charlotte'
		, lName: 'Bronte'
		, email: 'c.bronte@slampoets.jam' 
    , id: 1 
	}
	,{ 
		img: '8/85/Dantealighieri.PNG'
    , bio: 'Durante degli Alighieri, simply referred to as Dante, was a major Italian poet of the Middle Ages. His Divine Comedy, originally called Commedia and later called Divina by Boccaccio, is widely considered the greatest literary work composed in the Italian language and a masterpiece of world literature.'
		, fName: 'Dante'
		, lName: 'Alighieri'
		, email: 'd.alighieri@gmail.com' 
    , id: 2
	}
	,{ 
		img: 'c/c4/Susan_B_Anthony_c1855.png'
    , bio: 'was a prominent American civil rights leader who played a pivotal role in the 19th century'
		, fName: 'Susan B.'
		, lName: 'Anthony'
		, email: 's.b.anthony@gmail.com' 
    , id: 3 
	}
	,{ 
		img: '4/44/Washington_Irving.jpg'
    , bio: 'Washington Irving was an American author, essayist, biographer, historian, and diplomat of the early 19th century. He is best known for his short stories "The Legend of Sleepy Hollow" and "Rip Van Winkle", both of which appear in his book...'
		, fName: 'Washington'
		, lName: 'Irving'
		, email: 'w.irving@yahoo.com' 
    , id: 4 
	}
	,{ 
		img: 'd/d4/Jane_Austen_coloured_version.jpg'
    , bio: 'Jane Austen (16 December 1775 – 18 July 1817) was an English novelist whose works of romantic fiction, set among the landed gentry, earned her a place as one of the most widely read writers in English literature. Her realism and biting social commentary have gained her historical importance among scholars and critics.'
		, fName: 'Jane'
		, lName: 'Austen'
		, email: 'j.austen@myfreeemailaccount.ru'
    , id: 5 
	}
	,{ 
		img: 'b/b4/Hw-shakespeare.jpg'
    , bio: 'William Shakespeare was an English poet and playwright, widely regarded as the greatest writer in the English language and the world\'s pre-eminent dramatist. He is often called England\'s national poet and the "Bard of Avon".'
		, fName: 'William'
		, lName: 'Shakespeare'
		, email: 'w.shakespeare@googlemail.co.uk'
    , id: 6 
	}
	,{ 
		img: 'a/a7/Charles_Dickens2.jpg'
    , bio: 'Charles Dickens was an English writer and social critic. He created some of the world\'s most memorable fictional characters and is generally regarded as the greatest novelist of the Victorian period. During his life, his works enjoyed unprecedented fame...'
		, fName: 'Charles'
		, lName: 'Dickens'
		, email: 'c.dickens@geocities.com'
    , id: 7 
	}
	,{ 
		img: '3/3c/Fanny_Fern.png'
    , bio: 'Fanny Fern was an American newspaper columnist, humorist, novelist, and author of children\'s stories in the 1850s-1870s. Fern\'s great popularity has been attributed to her conversational style and sense of what mattered to her mostly middle-class female readers.'
		, fName: 'Fanny'
		, lName: 'Fern'
		, email: 'f.fern@gmail.com'
    , id: 8 
	}
	,{ 
		img: '0/0c/Benjamin_Disraeli_by_Cornelius_Jabez_Hughes%2C_1878.jpg'
    , bio: 'Benjamin Disraeli was a British Prime Minister, parliamentarian, Conservative statesman and literary figure. He served in government in four decades, twice as Prime Minister of Great Britain.'
		, fName: 'Benjamin'
		, lName: 'Disraeli'
		, email: 'b.disraeli@yahoo.com'
    , id: 9 
	}
	,{ 
		img: 'd/d0/Edna_St._Vincent_Millay_1933_van_Vechten.jpg'
    , bio: 'Edna St. Vincent Millay was an American lyrical poet and playwright. She received the Pulitzer Prize for Poetry in 1923, the third woman to win the award for poetry,[2] and was also known for her feminist activism and her many love affairs.'
		, fName: 'Edna'
		, lName: 'St. Vincent Millay'
		, email: 'e.sv.millay@hotmail.com'
    , id: 10 
	}
	,{ 
		img: 'b/bb/Conan_doyle.jpg'
    , bio: 'Sir Arthur Conan Doyle was a Scottish physician and writer, most noted for his stories about the detective Sherlock Holmes, generally considered milestones in the field of crime fiction, and for the adventures of Professor Challenger.'
		, fName: 'Sir Arthur'
		, lName: 'Conan Doyle'
		, email: 'a.c.doyle@gmail.com'
    , id: 11
	}
	,{ 
		img: '7/71/Marie_Curie_c1920.png'
    , bio: 'Marie Curie was a Polish physicist and chemist, working mainly in France, who is famous for her pioneering research on radioactivity. '
		, fName: 'Marie'
		, lName: 'Curie'
		, email: 'm.curie@yahoo.com'
    , id: 12
	}
	,{ 
		img: '5/5d/Alfred_Lord_Tennyson_1869.jpg'
    , bio: 'Alfred Lord Tennyson was Poet Laureate of Great Britain and Ireland during much of Queen Victoria\'s reign and remains one of the most popular British poets.'
		, fName: 'Alfred Lord'
		, lName: 'Tennyson'
		, email: 'al.tennyson@googlemail.co.uk'
    , id: 13
	}
	,{ 
		img: 'e/e8/WcbbustCBarton2.jpg'
    , bio: 'Clara Barton was a pioneer American teacher, patent clerk, nurse, and humanitarian. At a time when relatively few women worked outside the home, Barton built a career helping others.'
		, fName: 'Clara'
		, lName: 'Barton'
		, email: 'c.barton@hotmail.com'
    , id: 14
	}
	,{ 
		img: '2/24/Salvador_Dalí_1939.jpg'
    , bio: 'Salvador Dalì was a prominent Spanish surrealist painter born in Figueres, Spain.'
		, fName: 'Salvador'
		, lName: 'Dalì'
		, email: 's.dali@gmail.com'
    , id: 15
	}
	,{ 
		img: '9/99/Margaret_Mead_%281901-1978%29.jpg'
    , bio: 'Margaret Mead was an American cultural anthropologist, who was frequently a featured writer and speaker in the mass media throughout the 1960s and 1970s.[2] She earned her bachelor degree at Barnard College in New York City, and her M.A. and Ph.D. degrees from Columbia University.'
		, fName: 'Margaret'
		, lName: 'Mead'
		, email: 'm.mead@gmail.com'
    , id: 16
	}
	,{ 
		img: 'f/f1/Stanley_Kunitz.jpg'
    , bio: 'Stanley Kunitz was an American poet. He was appointed Poet Laureate Consultant in Poetry to the Library of Congress twice, first in 1974 and then again in 2000.'
		, fName: 'Stanley'
		, lName: 'Kunitz'
		, email: 's.kunitz@hotmail.com'
    , id: 17
	}
	,{ 
		img: '0/09/Martha_Graham_1948.jpg'
    , bio: 'Martha Graham was an American modern dancer and choreographer whose influence on dance has been compared with the influence Picasso had on the modern visual arts, Stravinsky had on music, or Frank Lloyd Wright had on architecture.'
		, fName: 'Martha'
		, lName: 'Graham'
		, email: 'm.graham@yahoo.com'
    , id: 18
	}
	,{ 
		img: '5/5d/Elon_Musk_in_Mission_Control_at_SpaceX.jpg'
    , bio: 'Elon Musk is a South African-born American entrepreneur. He is best known for founding SpaceX, and co-founding Tesla Motors and PayPal'
		, fName: 'Elon'
		, lName: 'Musk'
		, email: 'e.musk@spacex.com'
    , id: 19
	}
	,{ 
		img: '1/11/Gary_Francione.jpg'
    , bio: 'Gary Francione is an American legal scholar. He is the Distinguished Professor of Law and Nicholas deB. Katzenbach Scholar of Law & Philosophy at Rutgers School of Law-Newark.'
		, fName: 'Gary'
		, lName: 'Francione'
		, email: 'g.francione@yahoo.com'
    , id: 20
	}
	,{ 
		img: '2/27/Marilyn_Monroe_-_publicity.JPG'
    , bio: 'Marilyn Monroe was an American actress, model, and singer, who became a major sex symbol, starring in a number of commercially successful motion pictures during the 1950s and early 1960s.'
		, fName: 'Marilyn'
		, lName: 'Monroe'
		, email: 'm.monroe@hotmail.com'
    , id: 21
	}
	,{ 
		img: '2/2d/Amelia_earhart.jpeg'
    , bio: 'Amelia Earhart was an American aviation pioneer and author. Earhart was the first female pilot to fly solo across the Atlantic Ocean. She received the U.S. Distinguished Flying Cross for this record.'
		, fName: 'Amelia'
		, lName: 'Earhart'
		, email: 'a.earhart@spacex.com'
    , id: 22
	}
	,{ 
		img: 'b/b0/John_Updike_with_Bushes_new.jpg'
    , bio: 'John Updike was an American novelist, poet, short story writer, art critic, and literary critic.'
		, fName: 'John'
		, lName: 'Updike'
		, email: 'j.updike@aol.com'
    , id: 23
	}
	,{ 
		img: '1/13/Millicent_Fawcett.jpg'
    , bio: 'Millicent Fawcett was an English suffragist and an early feminist. She was a British suffragist, an intellectual, political leader, Union leader, mother, wife and writer.'
		, fName: 'Millicent'
		, lName: 'Fawcett'
		, email: 'm.fawcett@aol.com'
    , id: 24
	}
	,{ 
		img: '9/93/Benazir_bhutto_1988.jpg'
    , bio: 'Benazir Bhutto was a politician and stateswoman who served as the 11th Prime Minister of Pakistan in two non-consecutive terms from November 1988 until October 1990, and 1993 until her final dismissal on November 1996.'
		, fName: 'Benazir'
		, lName: 'Bhutto'
		, email: 'b.bhutto@gmail.com'
    , id: 25
	}
]
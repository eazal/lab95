# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

$(window).ready ->
	$('#loading').hide()
	$($('.piece')[0]).show()

window.mg =
	piece: $ '.piece'
	allResponsesValid: false
	advance: ->
		mg.step++
		mg.meta = ''
		mg.allResponsesValid = false
		mg.processStep = 0
		$(mg.piece).hide()
		$(mg.piece[mg.step]).show()
		$('.progress .bar').css('width', mg.step/(mg.piece.length - 1) * 100 + '%')
	step: 0
	totalMods: App.totalMods
	processStep: 0
	submitAnswer: (answer, mmgid, modid, qid, processLength, meta) ->
		processLength = ( processLength || 1 )
		url = '/raw/mods/' + modid + '/questions/' + qid + '/answers.json' 
		data =
			answer:
				answer: answer
				modulegroup_mod_id: mmgid
				mod_id: modid
				question_id: qid
				meta: meta
		$.ajax(
			url: url
			type: 'POST'
			data: data
			success: (data) ->
				mg.processStep++
				if mg.processStep == processLength
				  if mg.allResponsesValid then mg.advance() else mg.processStep = 0
			error: (data) ->
				console.log(data)
		)
	parseAnswerArray: (responseSet) ->
		mg.submitAnswer response.answer, response.mmgid, response.modid, response.qid, responseSet.length, mg.meta for response, i in responseSet
	meta: ''
	setMeta: (meta) ->
		mg.meta = meta



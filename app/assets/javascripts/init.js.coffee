setCookie = (name, value, days) ->
    if days
        date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000))
        expires = "; expires=" + date.toGMTString()
    else 
    	expires = ""
    document.cookie = escape(name) + "=" + escape(value) + expires + "; path=/"

setCookie('XSRF-TOKEN', $('meta[name="csrf-token"]').attr('content'), 30)
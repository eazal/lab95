# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

$('#logout').click ->
  $.ajax
    type: 'DELETE'
    url: '/users/sign_out'
    success: (data) ->
    	location.href = '/'
  return
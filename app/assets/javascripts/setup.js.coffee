window.App = @

angular.module('mods', ['moduleServices'])
	.config(['$routeProvider', ($routeProvider) ->
		$routeProvider
	  		.when('/', { templateUrl: 'partials/index.html' })
	  		.when('/mods/new', { templateUrl: 'partials/mod-new.html', controller: App.ModuleNewCtrl })
	  		.when('/mods', { templateUrl: 'partials/mod-list.html', controller: App.ModuleDetailListCtrl })
	      	.when('/mods/:modId', { templateUrl: 'partials/mod-detail.html', controller: App.ModuleDetailCtrl })
	  		.when('/modulegroups/new', { templateUrl: 'partials/modulegroup-new.html', controller: App.ModuleGroupNewCtrl })
	  		.when('/modulegroups', { templateUrl: 'partials/modulegroup-list.html', controller: App.ModuleDetailListCtrl })
	      	.when('/modulegroups/:modulegroupId', { templateUrl: 'partials/modulegroup-detail.html', controller: App.ModuleGroupDetailCtrl })
	])
	.run(['$http', ($http) ->
		$http.defaults.headers.common['X-CSRF-Token'] = $('meta[name="csrf-token"]').attr('content')
	])
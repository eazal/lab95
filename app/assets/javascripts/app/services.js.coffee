angular.module('moduleServices', ['ngResource'])
	.factory('Index', ['$resource', ($resource) ->
		return $resource('raw/:obj.json', {}, {
			query: { method: 'GET', params: { obj: 'mods' }, isArray:true }
		})
	]
	)
	.factory('Mod', ['$resource', ($resource) ->
		# This concatenation allows it to work with no :modId present
		return $resource('raw/mods/:modId' + '.json'
		, { 
			modId: '@modId'
		}
		, { 
			update: { method: 'PUT' } 
		}
		)
	]
	)
	.factory('Modulegroup', ['$resource', ($resource) ->
		return $resource('raw/modulegroups/:modulegroupId' + '.json'
		, { 
			modulegroupId: '@modulegroupId' 
			mod_id: '@modId'
		}
		, { 
			update: { method: 'PUT' } 
		}
		)
	]
	)
    .factory('Modulegroup_mod', ['$resource', ($resource) ->
        return $resource('raw/modulegroup_mods/:id' + '.json'
        , { 
                id: '@id' 
                modId: '@modId'
        }
        , { 
                update: { method: 'PUT' } 
        }
        )
    ]
    )
	.factory('Mod_modulegroup_mod', ['$resource', ($resource) ->
		return $resource('raw/modulegroups/:modulegroup_id/modulegroup_mods/:id' + '.json'
		, { 
			id: '@id' 
			modulegroup_id: '@modulegroup_id'
		}
		, { 
			update: { method: 'PUT' } 
		}
		)
	]
	)
	.factory('Modulegroup_mod_mod', ['$resource', ($resource) ->
		return $resource('raw/modulegroup_mods/:modulegroup_mod_id/mods/:id' + '.json'
		, { 
			id: '@id' 
			modulegroup_mod_id: '@modulegroup_id'
		}
		, { 
			update: { method: 'PUT' } 
		}
		)
	]
	)
	.factory('Question', ['$resource', ($resource) ->
		return $resource('raw/mods/:modId/questions/:questionId' + '.json'
		, { 
			modId: '@modId' 
			, questionId: '@questionId'
		}
		, { 
			update: { method: 'PUT' } 
		}
		)
	]
	)
	.factory('Option', ['$resource', ($resource) ->
		return $resource('raw/mods/:modId/questions/:questionId/options/:optionId' + '.json'
		, { 
			modId: '@modId' 
			, questionId: '@questionId'
			, optionId: '@optionId'
		}
		, { 
			update: { method: 'PUT' } 
		}
		)
	]
	)
	.factory('Globals', () ->
		page =
			name: undefined
		return { 
			thisMenuItem: { id: null } 
			getPage: ->
				return page
		}
	)

App.popArrayById = (array, id) ->
	array.splice(array.map((x) -> x.id).indexOf(id), 1)
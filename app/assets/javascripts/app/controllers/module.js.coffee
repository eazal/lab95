# Throttle

debounce = (fn, delay, c) ->
	c = ( c || this )
	timer = null
	return () ->
		context = c
		args = arguments
		clearTimeout(timer)
		timer = setTimeout(() ->
			fn.apply(context, args)
		, delay
		)

App.graphableModTypes = ['ps', 'ab']
App.graphableQuestionTypes = ['radio', 'checkbox']

# MODULE LIST

App.ModuleListCtrl = ($scope, $route, $routeParams, $http, $location, Index, Mod, Modulegroup, Question, Option, Globals) ->
	$scope.thisPage = Globals.getPage()
	setTimeout(->
		$scope.setThisModMenuItem($routeParams.modId) if $routeParams.modId
		$scope.setThisMgMenuItem($routeParams.modulegroupId) if $routeParams.modulegroupId
	, 200
	)
	$scope.modSearch				= false
	$scope.toggleModSearch			= () -> 
		$scope.modSearch = !$scope.modSearch
		$scope.modQuery = ''
	$scope.mgSearch					= false
	$scope.toggleMgSearch			= () -> 
		$scope.mgSearch = !$scope.mgSearch
		$scope.mgQuery = ''
	$scope.mods 					= Mod.query()

	$scope.modulegroups				= Modulegroup.query()
	$scope.orderProp 				= 'updated_at'
	$scope.thisMenuItem 			= Globals.thisMenuItem

	# TODO: Move this into a directive instead.
	$scope.setThisModMenuItem 			= (id, name) ->
		$scope.thisMenuItem.id 			= 'm' + id
		$scope.thisPage.name 			= name
	    # TODO Change this to AJS $location
		location.href = '/#/mods/' + id
	$scope.setThisMgMenuItem 			= (id, name) ->
		$scope.thisMenuItem.id 			= 'mg' + id
		$scope.thisPage.name 			= name
	    # TODO Change this to AJS $location
		location.href = '/#/modulegroups/' + id
	$scope.create = () ->
		_ = @
		_.id = 0
		_.mgid = 0
		$scope.mods.push(
			Mod.save(
				name: 'Untitled Module'
				, archived: false
			, (data) -> 
				_.id = data.id
				$scope.setThisModMenuItem(_.id, 'Untitled Module')
				Modulegroup.save(
					name: ''
					, mod_id: _.id
				, (data) ->
					_.mgid = data.id
					Mod.update(
						modId: _.id
						mod:
							key_module_group: _.mgid
					)
				)
			)
		)
		setTimeout(()->
			$('#mod-' + _.id).focus()
			$('#mod-' + _.id).select()
		, 200
		)
	$scope.createModulegroup = () ->
		_ = @
		_.id = 0
		Modulegroup.save(
			name: 'Untitled Module Group'
			mods: []
		, (data) ->
			_.id = data.id
			$scope.modulegroups.push(data)
			location.href = "/#/modulegroups/" + data.id
			setTimeout(()->
				$('#mg-' + _.id).focus()
				$('#mg-' + _.id).select()
			, 200
			)
		)

	$scope.qTypes = [
		code: 'radio'
		name: 'Radio Buttons'
	,
		code: 'checkbox'
		name: 'Checkboxes'
	,
		code: 'input'
		name: 'Single line of text'
	,
		code: 'textarea'
		name: 'Paragraph of text'
	]
	$scope.setQType = (question, code, modId) ->
		params = new Object()
		params.id = modId
		params.question = question
		params.question.qtype = code
		$scope.sync('q', 'q23940923', params)
		$scope.debounce()
	$scope.getQType = (qtype) ->
		qTypeIndex = 0
		$.grep($scope.qTypes, (e, i) -> qTypeIndex = i if e.code == qtype )
		return $scope.qTypes[qTypeIndex]


	# SERVER SYNCING MAGIC HAPPENS HERE

	# Queue of updates to make on debounce
	$scope.updates = []
	_$scope = 
		updates: []

	$scope.debounce = debounce(
		() -> 
			_$scope.updates = $scope.updates
			$scope.updates = []
			prune(_$scope.updates)
	, 700, false
	)

	# Push an update to array, replace if already exists
	$scope.sync = (obj, changeid, params) -> 
		update =
			obj: obj
			changeid: parseInt(changeid)
			params: params
		$scope.updates.push(update)

	`prune = function(_updates) {
	var i, item, _i, _len;
	_updates = _updates.sort(function(a, b) {
	return a.changeid - b.changeid;
	});
	for (i = _i = 0, _len = _updates.length; _i < _len || function(){$scope.massUpdate(_updates)}(); i = ++_i) {
	item = _updates[i];
	if (typeof _updates[i + 1] === 'object') {
	  if (_updates[i].changeid === _updates[i + 1].changeid) {
	    delete _updates[i];
	  }
	}
	}
	
	};`

	# Run all the updates
	$scope.massUpdate = (updates) ->
		updates = updates.filter((n) -> return n)
		(
			switch item.obj
				when 'm'
					mod =
						name: item.params.name
						, modId: item.params.id
					Mod.update(
						mod
					, (data) -> 
						App.logger.save()
					)
					break
				when 'mg'
					mg =
						name: item.params.name
						, modulegroupId: item.params.id
					Modulegroup.update(
						mg
					, (data) ->
						App.logger.save()
					)
				when 'q'
					question = 
						modId: item.params.id
						questionId: item.params.question.id
						name: item.params.question.name
						qtype: item.params.question.qtype
						description: item.params.question.description
					Question.update(
						question
					, (data) ->
						App.logger.save()
					)
				when 'o'
					option =
						name: item.params.question.option.name
						optionId: item.params.question.option.id
						questionId: item.params.question.id
						modId: item.params.id
					Option.update(
						option
					, (data) -> 
						App.logger.save()
					)
					break
		) for item, i in updates

	App.logger =
		save: () ->
			console.log('%c Save attempted', 'color: green;font-weight:bold;')
			_$scope.updates = []
App.ModuleListCtrl.$inject = ['$scope', '$route', '$routeParams', '$http', '$location', 'Index', 'Mod', 'Modulegroup', 'Question', 'Option', 'Globals']

# MODULE DETAIL

App.ModuleDetailCtrl = ($scope, $routeParams, $location, $route, Mod, Modulegroup, Modulegroup_mod, Modulegroup_mod_mod, Mod_modulegroup_mod, Question, Option, Globals) ->
	$scope.thisPage = Globals.getPage()
	$scope.mod 			= Mod.get({ modId: $routeParams.modId })
	$scope.isResultable = (mType) -> if App.graphableModTypes.indexOf(mType) > -1 then return true else return false
	$scope.isGraphable = (qType) -> if App.graphableQuestionTypes.indexOf(qType) > -1 then return true else return false
	$scope.delete = (id) ->
		Mod.delete(
			modId: id
		, (data) ->
			location.href = '/'
		) if confirm "Are you sure you want to delete this Module?"
	$scope.pageName 	= Globals.pageName

	$scope.editMode = true
	$scope.toggleMode = (val) ->
		$scope.editMode = val

	$scope.modulegroup = new Object()
	setTimeout(() ->
		$scope.modulegroup.id = $scope.mod.key_module_group
		$scope.modulegroup_mods = Mod_modulegroup_mod.query({ modulegroup_id: $scope.mod.key_module_group })
	,
	1000
	)
	$scope.modulegroup_mod_mods = (modulegroup_mod_id) -> 
		Modulegroup_mod_mod.get(
			{ modulegroup_mod_id: modulegroup_mod_id }
		)

	$scope.addQuestion = () ->
		_ = @
		_.id = 0
		Question.save(
			modId: $scope.mod.id
			question: 
				mod_id: $scope.mod.id
				name: 'New ' + $scope.questionName()
				qtype: 'radio'
				archived: false
		, (data) -> 
			data.options = []
			$scope.mod.questions.push(data)
			_.id = data.id
			setTimeout(()->
				$('#q-' + _.id).focus()
				$('#q-' + _.id).select()
			, 300)
		)
	$scope.questionName = ->
		if ($scope.mod.mtype == 'vb') then 'vote bubble' else 'question'
	$scope.deleteQuestion = (modId, questionId) ->
		Question.delete(
			modId: modId
			questionId: questionId
		, (data) ->
			App.popArrayById($scope.mod.questions, questionId)
		)

	$scope.addOption = (qid, first) ->
		_ = @
		_.id = 0
		qIndex = 0
		$.grep($scope.mod.questions, (e, i) -> qIndex = i if e.id == qid )
		console.log(qIndex)
		Option.save(
			modId: $scope.mod.id
			questionId: qid
			option: 
				name: 'New Option'
				question_id: qid
		, (data) -> 
			_.id = data.id
			console.log($scope.mod.questions[qIndex])
			$scope.mod.questions[qIndex].options.push(data)
			setTimeout(()->
				$('#opt-' + _.id).focus()
				$('#opt-' + _.id).select()
			, 200) if(!first)
		)
	$scope.deleteOption = (modId, questionId, optionId) ->
		qIndex = 0
		$.grep($scope.mod.questions, (e, i) -> qIndex = i if e.id == questionId )
		Option.delete(
			modId: modId
			questionId: questionId
			optionId: optionId
		, (data) ->
			App.popArrayById($scope.mod.questions[qIndex].options, optionId)
		)
	$scope.setMType = (mtype) ->
		mod = 
			modId: $scope.mod.id
			mtype: mtype
		Mod.update(
			mod
		, (data) ->
			$route.reload()
			$scope.addQuestion()
		)
	$scope.setModImage = (alt) ->
		filepicker.setKey('ARZTJJB7QqCvy9AouFHcwz')
		filepicker.pickAndStore( 
			mimetype: "image/*"
		,
			location: "S3"
			(InkBlobs) ->
				convertedImage = new Object()
				filepicker.convert(InkBlobs[0],
					width: 1080
					height: 760
					fit: 'max'
				, (convertedImage) ->
					convertedImage = convertedImage
					mod =
						modId: $scope.mod.id
					if alt
						mod.filepicker_url_alt = convertedImage.url
					else
						mod.filepicker_url = convertedImage.url
					Mod.update(
						mod
					, (data) ->
						$route.reload()
					)
				)
			, (FPError) ->
				console.log(FPError)
			)
	$scope.setOptionImage = (qid, oid) ->
		filepicker.setKey('ARZTJJB7QqCvy9AouFHcwz')
		filepicker.pickAndStore( 
			mimetype: "image/*"
		,
			location: "S3"
			(InkBlobs) ->
				convertedImage = new Object()
				filepicker.convert(InkBlobs[0],
					width: 500
					height: 500
					fit: 'max'
				, (convertedImage) ->
					convertedImage = convertedImage
					option =
						questionId: qid
						optionId: oid
						modId: $scope.mod.id
						option:
							filepicker_url: convertedImage.url
					Option.update(
						option
					, (data) ->
						qIndex = undefined
						oIndex = undefined
						$.grep($scope.mod.questions, (e, i) -> qIndex = i if e.id == qid )
						$.grep($scope.mod.questions[qIndex].options, (e, i) -> oIndex = i if e.id == oid )
						$scope.mod.questions[qIndex].options[oIndex].filepicker_url = convertedImage.url
					)
				)
			, (FPError) ->
				console.log(FPError)
			)
	$scope.deleteImageOption = (qid, oid) ->
		Option.update(
			modId: $scope.mod.id
			questionId: qid
			optionId: oid
			option:
				filepicker_url: ''
		, (data) ->
			qIndex = undefined
			oIndex = undefined
			$.grep($scope.mod.questions, (e, i) -> qIndex = i if e.id == qid )
			$.grep($scope.mod.questions[qIndex].options, (e, i) -> oIndex = i if e.id == oid )
			$scope.mod.questions[qIndex].options[oIndex].filepicker_url = ''
		)
	$scope.modulegroups = Modulegroup.query()
	$scope.createModulegroup = () ->
		_ = @
		_.id = 0
		_.mgid = 0
		_.name = prompt('Name of new module group')
		$scope.modulegroups.push(
			Modulegroup.save(
				name: _.name
				mods: []
			, (data) ->
				newId = $scope.modulegroups.map((x) -> x.id).indexOf(data.id)
				$scope.modulegroups[newId].mods = []
			)
		)
	$scope.deleteModulegroup = (id) ->
		$scope.modulegroups.splice(
			$scope.modulegroups.indexOf(
				Modulegroup.delete(
					id: id
				)
			, 1
			)
		) if confirm "Are you sure you want to delete this Modulegroup?"

	$scope.addModToModgroup = (mgid) ->
		mgIndex = 0
		$.grep($scope.modulegroups, (e, i) -> mgIndex = i if e.id == mgid )
		Modulegroup_mod.save(
			modulegroup_mod:
				modulegroup_id: mgid
				mod_id: $scope.mod.id
		, (data) ->
			App.logger.save()
			$scope.modulegroups[mgIndex].mods.push($scope.mod)
		)

	$scope.removeModFromModgroup = (mgid, modid) ->
		mgIndex = 0
		$.grep($scope.modulegroups, (e, i) -> mgIndex = i if e.id == mgid )
		Modulegroup_mod.delete(
			id: mgid
			modId: modid
		, (data) ->
			App.logger.save()
			App.popArrayById($scope.modulegroups[mgIndex].mods, modid)
		)
	$scope.answersForOption = (option, qid, split, modid, mmgid) ->
		qIndex = 0
		count = 0
		$.grep($scope.mod.questions, (e, i) -> qIndex = i if e.id == qid )
		((count++ if ((split==answer.meta || !split)) && answer.modulegroup_mod_id == mmgid) if ((option.name || '') + (option.filepicker_url || '')) == answer.answer) for answer, i in $scope.mod.questions[qIndex].answers
		return count/$scope.mod.questions[qIndex].answers.length * 100 + '%'

App.ModuleDetailCtrl.$inject = ['$scope', '$routeParams', '$location', '$route', 'Mod', 'Modulegroup', 'Modulegroup_mod', 'Modulegroup_mod_mod', 'Mod_modulegroup_mod', 'Question', 'Option', 'Globals']
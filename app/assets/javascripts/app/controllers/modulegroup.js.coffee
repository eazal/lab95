App.ModuleGroupDetailCtrl = ($scope, $routeParams, $location, $route, Modulegroup, Mod, Modulegroup_mod, Modulegroup_mod_mod, Mod_modulegroup_mod, Globals) ->
	$scope.thisPage = Globals.getPage()
	$scope.modulegroup = Modulegroup.get({ modulegroupId: $routeParams.modulegroupId })
	$scope.modulegroup_mods = Mod_modulegroup_mod.query({ modulegroup_id: $routeParams.modulegroupId })
	$scope.modulegroup_mod_mods = (modulegroup_mod_id) -> 
		Modulegroup_mod_mod.get(
			{ modulegroup_mod_id: modulegroup_mod_id }
		)
	$scope.mods = Mod.query()
	$scope.addModToModgroup = (mod_id) ->
		Modulegroup_mod.save(
			modulegroup_mod:
				modulegroup_id: $scope.modulegroup.id
				mod_id: mod_id
		, (data) ->
			App.logger.save()
			Mod.get(
				{modId: mod_id}
			, (data) ->
				console.log(data)
				$scope.modulegroup.mods.push(data)
			)
		)
	$scope.removeModFromModgroup = (mod_id) ->
		Modulegroup_mod.delete(
			id: $scope.modulegroup.id
			modId: mod_id
		, (data) ->
			App.logger.save()
			popArrayById($scope.modulegroup.mods, mod_id)
		)
	$scope.delete = () ->
		Modulegroup.delete(
			modulegroupId: $scope.modulegroup.id
		, (data) ->
			location.href = '/'
		) if confirm "Are you sure you want to delete this Module Group? Modules inside this group will not be deleted."
	$scope.editMode = true
	$scope.toggleMode = (val) ->
		$scope.editMode = val
	$scope.answersForOption = (option, qid, split, modid, mmgid) ->
		qIndex = 0
		mIndex = 0
		count = 0
		$.grep($scope.modulegroup.mods, (e, i) -> mIndex = i if e.id == modid )
		$.grep($scope.modulegroup.mods[mIndex].questions, (e, i) -> qIndex = i if e.id == qid )
		((count++ if ((split==answer.meta || !split)) && answer.modulegroup_mod_id == mmgid) if ((option.name || '') + (option.filepicker_url || '')) == answer.answer) for answer, i in $scope.modulegroup.mods[mIndex].questions[qIndex].answers
		return count/$scope.modulegroup.mods[mIndex].questions[qIndex].answers.length * 100 + '%'
	$scope.isResultable = (mType) -> if App.graphableModTypes.indexOf(mType) > -1 then return true else return false
	$scope.isGraphable = (qType) -> if App.graphableQuestionTypes.indexOf(qType) > -1 then return true else return false

App.ModuleGroupDetailCtrl.$inject = ['$scope', '$routeParams', '$location', '$route', 'Modulegroup', 'Mod', 'Modulegroup_mod', 'Modulegroup_mod_mod', 'Mod_modulegroup_mod', 'Globals']
# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

window.suid = $('#suid').val()
window.stid = $('#stid').val()
window.sbid = 0
if ($('#meta').length > 0)
  meta = $('#meta').val()
else
  meta = ''

$('#take-submit').click ->
  $('.alert').hide()
  $.growl({ 
     title: 'Submitting...'
     , text: '<br />Please wait a moment while we have a look over your results.'
     , type: 'info' 
     , class: 'submitting'
     , delay: 2000
  })
  parseResponses()
  # We assume by default CATAs are valid
  $('li.qt-cata').parent().parent().remove()
  
  # Just to be on the safe suide,
  surveyComplete = () -> 
    incrementSurveyCount() if $('ol li').length == 0
  setTimeout surveyComplete, 600
  incrementSurveyCount = () ->
    $.ajax({
      url: '/submissions/' + sbid + '/surveys/' + suid
      , type: 'POST'
      , data: { meta: meta }
      , success: () ->
        $('#fullSurvey').hide()
        $('#thankYou').show()
    })
    


window.parseResponses = () ->
   response = ''
   markField = (id, status) ->
     statusMsg = ''
     statusIcon = 'ok'
     switch status
        when "success"
           statusMsg = 'Response submitted successfully'
           statusIcon = 'ok'
        when "danger"
           statusMsg = 'Please check your response'
           statusIcon = 'remove'
        when "info"
           statusMsg = 'We had trouble submitting this response due to <strong>a problem on our end</strong>. Please try again.'
           statusIcon = 'info-sign'
     $('.qid-' + id).before('<div class="alert alert-' + status + '" style="margin-left: -25px"><i class="icon-' + statusIcon + '"></i> ' + statusMsg + '</div>')
   sendResponse = (response, suid, qid, parentContainer) ->
      if response == undefined || response == ''
         markField(qid, 'danger')
      else
         $.ajax({
            type: 'POST'
            , url: '/studies/' + stid + '/surveys/' + suid + '/questions/' + qid + '/responses.json'
            , data: {
              answer: response 
              , submission_id: sbid
              , meta: meta
            }
            , success: (data) ->
               $(parentContainer).remove()
         })
   parseRadioButtons = (selector) ->
      for i in [0..$(selector).length-1] by 1
         thisField = $(selector)[i]
         console.log(thisField)
         response = $(thisField).find('input[type="radio"]:checked').siblings('span').html()
         parentContainer = $(thisField).parent().parent('li')
         qid = $(parentContainer).attr('class')
         qid = qid.substring(4, qid.length)
         sendResponse(response, suid, qid, parentContainer)

   # parse either/ors
   parseRadioButtons('.qt-eo')

   # parse fpt scale
   parseRadioButtons('.qt-fpt') 

   # parse input
   for i in [0..$('.qt-ts input[type="text"]').length-1] by 1
      thisField = $('.qt-ts input[type="text"]')[i]
      response = $(thisField).val() 
      parentContainer = $(thisField).parent().parent().parent('li')
      qid = $(parentContainer).attr('class')
      console.log(qid)
      qid = qid.substring(4, qid.length)
      sendResponse(response, suid, qid, parentContainer) 

   # parse textarea 
   for i in [0..$('.qt-tp textarea').length-1] by 1
      thisField = $('.qt-tp textarea')[i]
      response = $(thisField).val() 
      parentContainer = $(thisField).parent().parent().parent('li')
      qid = $(parentContainer).attr('class')
      qid = qid.substring(4, qid.length)
      sendResponse(response, suid, qid, parentContainer) 

   # parse check-all-that-applys 
   for i in [0..$('.qt-cata input[type="checkbox"]:checked').length-1] by 1
      thisField = $('.qt-cata input[type="checkbox"]:checked')[i]
      response = $(thisField).val()
      qid = $(thisField).parent().parent().parent().parent().parent().parent('li').attr('class')
      qid = qid.substring(4, qid.length)
      sendResponse(response, suid, qid) 

$('a.modal-link').click -> 
  url = $(this).attr('data-url')
  title = $(this).attr('data-title')
  $('.modal img#modal-body-img').attr('src', url)
  $('.modal h4').html(title)

window.imgLoaded = () ->
  $('.modal #loader').hide()

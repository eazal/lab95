class OptionsController < ApplicationController

	def create
		@question = Question.find(params[:question_id])
		@option = Option.new(params[:option])

		respond_to do |format|
			if @option.save
				format.json  { render :json => @option, :status => :created }
			else
				format.json  { render :json => @option.errors, :status => :unprocessable_entity }
			end
		end
	end

	def update
	    @mod = Mod.find(params[:mod_id])
	    @question = Question.find(params[:question_id])
	    @option = Option.find(params[:id])

	    respond_to do |format|
			if @option.update_attributes(params[:option])
				format.json  { render :json => @option, :status => :created, :location => [@mod, @question, @option] }
			else
				format.json  { render :json => @option.errors, :status => :unprocessable_entity }
			end
	    end
	end

	def destroy
		@option = Option.find(params[:id])
		@option.destroy

		respond_to do |format|
		  	format.json { head :no_content }
		end
	end

end

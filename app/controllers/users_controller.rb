class UsersController < ApplicationController
	before_filter :authenticate_user!

	def all
		@users = User.all

     respond_to do |format|
       format.html
       format.json { render :json => @users }
     end
	end

	def edit
		@user = User.find(params[:id])

		if current_user.level == 5
			respond_to do |format|
			format.html
			format.json { render :json => @user }
		end
    end
	end

	def check
		if user_signed_in?
			@status = true
			@user = current_user
		else
			@status = false
			@user = nil
		end
		respond_to do |format|
			format.json { render :json => [@user, @status] }
		end
	end

  def update
  	if current_user.level == 5
	    @user = User.find(params[:id])
	    @user.confirm!
	    if @user.update_attributes(params[:user])
			respond_to do |format|
				format.html { redirect_to '/users/all' }
				format.json { render :json => @user }
			end
	    end
	  end
  else
    if current_user.update_attributes(params[:user])
		respond_to do |format|
			format.json { render :json => @user }
		end
    end
  end

end

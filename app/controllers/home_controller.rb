class HomeController < ApplicationController

  before_filter :authenticate_user!, :only => [:ghost]

	def index
		respond_to do |format|
			format.html
		end
	end

	def redirect
		redirect_to '/'
	end
	  
	def ghost
	    return unless current_user.level === 5
	    sign_in(:user, User.find(params[:user_id]))
	    redirect_to '/'
	end

	def default
		respond_to do |format|
			format.html { render :layout => 'blank' }
		end
	end


end

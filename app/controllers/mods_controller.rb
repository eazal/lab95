class ModsController < ApplicationController
	# before_filter :authenticate_user!, :only => [:new]

	def index
		if(params.has_key?(:modulegroup_mod_id))
			@modulegroup_mod = ModulegroupMod.where(:id => params[:modulegroup_mod_id]).first
			@mod = @modulegroup_mod.mod
		else
			@mods = current_user.mods(:conditions => ["archived != ?", true])
		end

		respond_to do |format|
			if(params.has_key?(:modulegroup_mod_id))
				format.json { render :json => @mod.to_json(
					:include => { 
						:questions => {
							:include => [:options, :answers]
						}
					} 
				) }
			else
				format.json { render :json => @mods.to_json() }
			end
		end
	end

	def show
		@mod = Mod.find(params[:id])
		
		if @mod.user_id = current_user.id
			respond_to do |format|
				format.json { render :json => 
					@mod.as_json(
						:include => { 
							:questions => {
								:include => [:options, :answers]
							}
						} 
					) 
				}
			end
		end
	end

	def mod_detail
		respond_to do |format|
			format.html { render :layout => 'blank' }
		end
	end

	def new
	end

	def destroy
		@mod = Mod.find(params[:id])
		if @mod.user_id = current_user.id
			@mod.destroy
		end

		respond_to do |format|
			format.json { head :no_content }
		end
	end

	def create
		@mod = Mod.new(params[:mod])
		@mod.user_id = current_user.id

		respond_to do |format|
			if @mod.save
				format.json  { render :json => @mod,
				            :status => :created, :location => @mod }
			else
				format.json  { render :json => @mod.errors,
				            :status => :unprocessable_entity }
			end
		end
	end

	def update
		@mod = Mod.find(params[:id])

		respond_to do |format|
			if @mod.update_attributes(params[:mod])
				format.json  { render :json => @mod }
			else
				format.json  { render :json => @mod.errors,
			            :status => :unprocessable_entity }
			end
		end
	end

end
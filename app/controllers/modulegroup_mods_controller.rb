class ModulegroupModsController < ApplicationController

	def index
		# https://github.com/mixonic/ranked-model#complex-use
		if(params.has_key?(:id))
			@modulegroup_mods = ModulegroupMod.where(:modulegroup_id => params[:id])
		else
			@modulegroup_mods = ModulegroupMod.all().rank(:mod_rank)
		end
		
		respond_to do |format|
			format.json { render :json => @modulegroup_mods.to_json() }
		end
	end

	def show
		if(params.has_key?(:modId))
			@modulegroup_mod = ModulegroupMod.where(:modulegroup_id => params[:id], :mod_id => params[:modId]).first
			@array = @modulegroup_mods
		else
			@modulegroup_mods = ModulegroupMod.where(:modulegroup_id => params[:modulegroup_id])
			@array = @modulegroup_mods
		end

		respond_to do |format|
			format.json { render :json => @array.to_json }
		end
	end

	def destroy
		@modulegroup_mod = ModulegroupMod.where(:modulegroup_id => params[:id], :mod_id => params[:modId]).first
		@modulegroup_mod.destroy

		respond_to do |format|
			format.json { head :no_content }
		end
	end

	def create
		@modulegroup_mod = ModulegroupMod.new(params[:modulegroup_mod])

		respond_to do |format|
			if @modulegroup_mod.save
				format.json  { render :json => @modulegroup_mod,
				            :status => :created, :location => @modulegroup_mod }
			else
				format.json  { render :json => @modulegroup_mod.errors,
				            :status => :unprocessable_entity }
			end
		end
	end

end

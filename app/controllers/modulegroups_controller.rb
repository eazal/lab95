class ModulegroupsController < ApplicationController

	def index
		# @modulegroups = Modulegroup.select('modulegroups.*, count(mods.id) as mod_count')
		# 	.joins(:mods)
		# 	.group('mods.id')
		# 	.having('mod_count > 1')
		@modulegroups = Modulegroup.find(:all, :conditions => ['name != ? AND user_id = ?', '', current_user.id])

		respond_to do |format|
			format.json { render :json => 
				@modulegroups.to_json( :include => [ :mods ] )
			}
		end
	end

	def show
		@modulegroup = Modulegroup.find(params[:id])
		@ip = request.remote_ip.force_encoding("UTF-8")
		@modulegroup_mods = @modulegroup.modulegroup_mods
		@user_authorized = user_authorized(@modulegroup, @ip)

		respond_to do |format|
			format.html { render :layout => 'minimal' }
			format.json { render :json => 
				@modulegroup.to_json( 
					:include => {
						:mods => {
							:include => {
								:questions => {
									:include => :answers
								}
							}
						}
					} 
				)
			}
		end

		# def current_user_answer
		# 	if user_signed_in?
		# 	  return self.answers.where('user_id == ?, question_id == ? ', current_user.id, self.id)
		# 	else
		# 	  return self.answers.where('ip == ?, question_id == ? ', request.remote_ip.force_encoding("UTF-8"), self.id)
		# 	end
		# end
		# @current_user_answer = current_user_answer

	end

	def modulegroup_detail
		respond_to do |format|
			format.html { render :layout => 'blank' }
		end
	end

	def destroy
		@modulegroup = Modulegroup.find(params[:id])
		if @modulegroup.user_id = current_user.id
			@modulegroup.destroy
		end

		respond_to do |format|
			format.json { head :no_content }
		end
	end

	def create
		@modulegroup = Modulegroup.new(params[:modulegroup])
		if(params.has_key?(:mod_id))
			@mod = Mod.find(params[:mod_id])
			@modulegroup.mods << @mod
		end
		@modulegroup.user_id = current_user.id

		respond_to do |format|
			if @modulegroup.save
				format.json  { render :json => @modulegroup,
				            :status => :created, :location => @modulegroup }
			else
				format.json  { render :json => @modulegroup.errors,
				            :status => :unprocessable_entity }
			end
		end
	end

	def update
		@modulegroup = Modulegroup.find(params[:id])
		if(params.has_key?(:mod_id))
			@mod = Mod.find(params[:mod_id])
			@modulegroup.mods << @mod
		end

		respond_to do |format|
			if @modulegroup.update_attributes(params[:modulegroup])
			format.json  { render :json => @modulegroup,
			            :status => :created, :location => @modulegroup }
			else
				format.json  { render :json => @modulegroup.errors,
			            :status => :unprocessable_entity }
			end
		end
	end

	private
    def user_authorized(mg, ip)
    	# Default settings in case of error.
    	# @auth_authorized can be either true, false, or special.
    	if user_signed_in? && ( current_user.id == mg.user_id || current_user.level === 5 )
    		@auth_authorized = 'special'
    		@auth_message = 'You are the author of this Trial and may submit as many times as you wish.'
    		if current_user.level === 5
	    		@auth_message = 'You are a Super Administrator and may submit as many times as you wish.'
	    	end
	    else
	    	@auth_authorized = 'true'

	    	# TODO: Falsification logic
	    	# @auth_message = 'For an unknown reason, we could not authenticate you for this trial.'
    	end

    	@auth_obj = {
    		:authorized => @auth_authorized,
    		:message => @auth_message
    	}
    	return @auth_obj
    end
    helper_method :user_authorized

end

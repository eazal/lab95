class QuestionsController < ApplicationController

  before_filter :authenticate_user!, :only => [:new]

   def index
     @questions = Question.where(:mod_id => params[:mod_id])

     respond_to do |format|
       format.json { render :json => @questions.as_json }
     end
   end

  def all 
     @questions = Question.all
     @survey = Survey.all

     respond_to do |format|
       format.html # index.html.erb
       format.json { render :json => @questions }
     end
  end

  def destroy
    @question = Question.find(params[:id])
    @question.destroy

    respond_to do |format|
      format.json { head :no_content }
    end
  end

  class OptCount
    def initialize(options)
      @names = @options
    end
  end

  def show
    @study = Study.find(params[:study_id])
    @question = Question.find(params[:id])
    @answers = @question.responses
    @survey = Survey.find(params[:survey_id])

    if @survey.kind === 'aBTest'
      @answers_answers_a = @answers.where(:meta => '0').pluck(:answer) # Just an array of the answers each as a string
      @answers_answers_b = @answers.where(:meta => '1').pluck(:answer) # Just an array of the answers each as a string
    else
      @answers_answers = @answers.pluck(:answer) # Just an array of the answers each as a string
    end

    @options = @question.options
    @author = @survey.user_id
    
    respond_to do |format|
      format.html
      format.json  { render :json => [@survey, @question] }
    end
  end

  def download_excel
    @question = Question.find(params[:question_id])
    @answers = @question.responses
    @answers_answers = @answers.pluck(:answer) # Just an array of the answers each as a string
    @options = @question.options
    @survey = Survey.find(params[:survey_id])
  end

  def new 
    @study = Study.find(params[:study_id])
  	@survey = Survey.find(params[:survey_id])
    @question = Question.new
    @author = @survey.user_id

    respond_to do |format|
      format.html 
      format.json { render :json => @question }
    end
  end

  def edit 
    @study = Study.find(params[:study_id])
    @survey = Survey.find(params[:survey_id])
    @question = Question.find(params[:id])

    respond_to do |format|
      format.html
      format.json { render :json => @question }
    end
  end

  def create
    @mod = Mod.find(params[:mod_id])
    @question = Question.new(params[:question])

    respond_to do |format|
      if @question.save
        format.json  { render :json => @question,
                    :status => :created, :location => [@mod, @question] }
      else
        format.json  { render :json => @question.errors,
                    :status => :unprocessable_entity }
      end
    end
  end

  def update
    @mod = Mod.find(params[:mod_id])
    @question = Question.find(params[:id])

    respond_to do |format|
      if @question.update_attributes(params[:question])
      format.json  { render :json => @question,
                  :status => :created, :location => [@mod, @question] }
      else
        format.json  { render :json => @question.errors,
                  :status => :unprocessable_entity }
      end
    end
  end

end

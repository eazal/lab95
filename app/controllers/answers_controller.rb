class AnswersController < ApplicationController

	def create
		@alreadyAnswered = false
		@question_id = params[:question_id]
		@ip = request.remote_ip.force_encoding("UTF-8")
		@answer = nil

		if user_signed_in?
			@answer = Answer.where(:user_id => current_user.id, :question_id => @question_id).first
			if @answer.present?
				@alreadyAnswered = true
			else
				@answer = Answer.new(params[:answer])
				@answer.user_id = current_user.id
			end
			
		else
			@answer = Answer.where(:ip => @ip, :question_id => @question_id).first
			if @answer.present?
				@alreadyAnswered = true
			else
				@answer = Answer.new(params[:answer])
				@answer.ip = @ip
				@answer.user_id = 0
			end
		end

		logger.debug('======================================')
		logger.debug(@alreadyAnswered)
		logger.debug(@answer)

		# If an answer comes in for a user who has already answered, just update.


		respond_to do |format|
			if @alreadyAnswered
				if @answer.update_attributes(params[:answer])
					format.json  { render :json => @answer }
				else
					format.json  { render :json => @answer.errors,
				            :status => :unprocessable_entity }
				end
			else
				if @answer.save
					format.json  { render :json => @answer } 
				else
					format.json  { render :json => @answer.errors, :status => :unprocessable_entity }
				end
			end
		end
	end

	def chart
		@question = Question.find(params[:id])

		# TODO: This should generate perfect Google Chart API/gRaphael code
		@chart = {a: 'hello'}

		respond_to do |format|
			format.json { render :json => @chart.to_json }
		end

	end

end

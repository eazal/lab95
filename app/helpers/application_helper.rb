module ApplicationHelper
	def user_is_author(author)
		if user_signed_in?
			if current_user.level >= 5
				return true
			else
				if current_user.id == @author
					return true
				else
					return false
				end
			end
		else
			return false
		end
  end


  	def version
  		return '0.3.2'
  	end
	def my_email
	  return 'ajkochanowicz@gmail.com'
	end
	def my_name
		return 'Lab95'
	end
	def my_name_link
		return '/'
	end
	def my_twitter
		return ''
	end
	def site_title
		return "Lab95"
	end
	def site_description
		return "A suite of small tools for collecting data"
	end

end
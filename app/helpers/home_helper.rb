module HomeHelper
	def completion_percentage(completed, needed)
		return completed.to_f/needed * 100
	end
end
